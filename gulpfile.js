var gulp = require('gulp');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var notify = require("gulp-notify");

gulp.task('default', function(){
  gulp.watch('./scss/*.scss', ['sass']);  
  gulp.watch('./build/css/*.css', ['minify']);  
})

gulp.task('sass', function(){
  return gulp.src('./scss/*.scss');
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('build/css'));
  	.pipe(notify("SCSS Built Successfully!"));
});
 
gulp.task('minify', function () {
    return gulp.src('./build/css/*.css');
        .pipe(csso());
        .pipe(gulp.dest('./build/css/'));
    	.pipe(notify("CSS Minified Successfully!"));
});